<?php

namespace App\Listener;

use Psr\Container\ContainerInterface;
use Swoole\Server;
use Hyperf\Contract\StdoutLoggerInterface;

class TcpListener
{
    protected StdoutLoggerInterface $logger;

    public function __construct(ContainerInterface $container)
    {
        $this->logger = $container->get(StdoutLoggerInterface::class);
    }

    // 客户端连接时触发
    public function onConnect(Server $server, int $fd): void
    {
        $this->logger->info("客户端 {$fd} 连接成功");
        $server->send($fd, "欢迎连接到 Hyperf TCP 服务器\n");
    }

    // 客户端发送数据时触发 telnet 127.0.0.1 9501
    public function onReceive(Server $server, int $fd, int $reactor_id, string $data): void
    {
        $this->logger->info("收到客户端 {$fd} 的消息: " . trim($data));

        // 简单的命令处理
        switch (trim($data)) {
            case 'ping':
                $server->send($fd, "pong\n");
                break;
            case 'time':
                $server->send($fd, "服务器时间：" . date('Y-m-d H:i:s') . "\n");
                break;
            case 'bye':
                $server->send($fd, "再见！\n");
                $server->close($fd);
                break;
            default:
                $server->send($fd, "你说的是：" . trim($data) . "\n");
                break;
        }
    }

    // 客户端断开连接时触发
    public function onClose(Server $server, int $fd): void
    {
        $this->logger->info("客户端 {$fd} 断开连接");
    }
}
